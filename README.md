Program to display the price of Bitcoin in real time. To visualize the data, the Matploitlib library is used. To generate the data in real time, the Csv library is used and the [nomics-python](https://github.com/TaylorFacen/nomics-python) library is used to collect the data from the API of [Nomics](https://nomics.com)

To run the program it is recommended to use a virtual environment for Python as virtualenv and install the dependencies with:

`pip install -r requirements.txt`

and run each script on different terminals. To generate the data:

`python nomics_api.py`

And to see the graph in real time:

`python plot_live_data.py`

![example](example.png)
