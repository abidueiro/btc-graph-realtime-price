from nomics import Nomics
from datetime import datetime
import csv
import time
import os
from dotenv import load_dotenv
import os.path

load_dotenv()

API_KEY = os.getenv('API_KEY')

def get_crypto_price(coin):
    nomics = Nomics(API_KEY)
    markets = nomics.Currencies.get_currencies(ids = coin)
    price = markets[0]['price']
    price = float(price)
    text = round(price)
    return text

def main():
    last_price = -1
    filename = 'btc_price.csv'
    file_exists = os.path.isfile(filename) 
    if file_exists != True:
        with open(filename, 'w') as csv_file:
            fieldnames = ["date", "price"]
            csv_writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
            csv_writer.writeheader()
    else:
        pass

    while True:
            crypto = 'BTC'
            price = get_crypto_price(crypto)
            print(price)
            now = datetime.now()
            dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
            if price != last_price:
                with open('btc_price.csv', mode='a', newline='') as btc_price:
                    btc_writer = csv.writer(btc_price, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                    btc_writer.writerow([dt_string, price])
                last_price = price
            time.sleep(5)

main()